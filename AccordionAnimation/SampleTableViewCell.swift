//
//  SampleTableViewCell.swift
//  AccordionAnimation
//
//  Created by Varun on 07/07/16.
//  Copyright © 2016 YMediaLabs. All rights reserved.
//

import UIKit
import AccordionAnimation

class SampleTableViewCell: AccordionTableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
